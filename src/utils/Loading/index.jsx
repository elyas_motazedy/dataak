import Image from 'next/image';

const Loading = () => {
  return (
    <>
      <div className="justify-center flex-wrap items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        {/*content*/}
        <Image
          src="/dattak.jpg"
          alt="Dattak loading image"
          width="250"
          height="250"
          className="animate-pulse  "
        />
      </div>

      <div className="fixed inset-0 z-40 bg-white"></div>
    </>
  );
};

export default Loading;
