// First we need to import axios.js
import axios from 'axios';

import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

// Next we make an 'instance' of it
const axiosReq = axios.create({
  // .. where we make our configurations
  baseURL: publicRuntimeConfig.baseApiURL,
});

axiosReq.defaults.headers.post['Content-Type'] = 'application/json';

export default axiosReq;
