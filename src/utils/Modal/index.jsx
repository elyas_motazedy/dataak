import { useEffect } from 'react';

const Modal = ({ children, showModal }) => {
  useEffect(() => {
    document.body.className = showModal ? 'overflow-hidden' : '';
  }, [showModal]);
  return (
    <>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative my-6 mx-auto w-5/6 sm:w-5/6  md:w-3/4 lg:w-3/6 xl:2/6">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {children}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
