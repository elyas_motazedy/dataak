//posts
export const GET_POST = 'GET_POST';
export const GET_SINGLE_POST = 'GET_SINGLE_POST';

// Loading
export const SET_LOADING = 'SET_LOADING';
