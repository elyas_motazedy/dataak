import { SET_LOADING, GET_POST, GET_SINGLE_POST } from '../../types';

const initialState = {
  posts: [],
  singlePost: [],
  loading: false,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_POST:
      return {
        ...state,
        posts: payload,
      };
    case GET_SINGLE_POST:
      return {
        ...state,
        singlePost: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        loading: payload,
      };
    default:
      return state;
  }
}
