import { SET_LOADING, GET_POST, GET_SINGLE_POST } from '../../types';
import axiosReq from '../../../utils/axiosConfig';
export const getPosts = () => async (dispatch) => {
  try {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const res = await axiosReq.get(`/posts?_embed=answers`);

    if (res.status === 200) {
      dispatch({
        type: GET_POST,
        payload: res.data,
      });
    }

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  } catch (err) {
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};
export const getSinglePost = (id) => async (dispatch) => {
  try {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const res = await axiosReq.get(`/posts/${id}?_embed=answers`);

    if (res.status === 200) {
      dispatch({
        type: GET_SINGLE_POST,
        payload: res.data,
      });
    }

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  } catch (err) {
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};
export const createPost = (data) => async (dispatch) => {
  try {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const res = await axiosReq.post(`/posts`, data);

    if (res.status === 201) {
      dispatch(getPosts());
    }

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  } catch (err) {
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};
export const addAnswer = (data, id) => async (dispatch) => {
  try {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const res = await axiosReq.post(`/answers`, data);
    if (res.status === 201) {
      dispatch(getSinglePost(id));
    }

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  } catch (err) {
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};

export const vote = (answer) => async (dispatch) => {
  try {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const res = await axiosReq.put(`/answers/${answer.id}`, answer);

    if (res.status === 200) {
      dispatch(getSinglePost(answer.postId));
    }

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  } catch (err) {
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};
