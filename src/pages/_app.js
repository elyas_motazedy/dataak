import { wrapper } from '../redux/store';
import '@/styles/globals.css';
import Header from '@/layout/Header';
function MyApp({ Component, pageProps }) {
  return (
    <>
      <Header /> <Component {...pageProps} />
    </>
  );
}
export default wrapper.withRedux(MyApp);
