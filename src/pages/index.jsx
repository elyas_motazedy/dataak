import { useEffect } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPosts } from '../redux/actions/post';
import Card from '@/components/Card';
import Loading from '@/utils/Loading';
const Home = ({ getPosts, posts, loading }) => {
  useEffect(() => {
    getPosts();
  }, [getPosts]);

  return (
    <div>
      <Head>
        <title>Dattak</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="container mx-auto">
          {!loading ? (
            posts.length > 0 ? (
              posts.map((post) => (
                <Card key={post.id} post={post} type="question" />
              ))
            ) : (
              <p className="text-center text-xl  mt-9">
                سوالی برای نشان دادن وجود ندارد
              </p>
            )
          ) : (
            <Loading />
          )}
        </div>
      </main>
    </div>
  );
};

Home.prototype = {
  posts: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  getPosts: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  posts: state.post.posts,
  loading: state.post.loading,
});

export default connect(mapStateToProps, { getPosts })(Home);
