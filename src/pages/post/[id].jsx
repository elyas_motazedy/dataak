import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSinglePost, addAnswer } from '@/redux/actions/post';
import Card from '@/components/Card';
import Loading from '@/utils/Loading';
const today = new Date();
const SinglePost = ({ getSinglePost, singlePost, loading, addAnswer }) => {
  const router = useRouter();
  const { id } = router.query;
  const [data, setData] = useState({
    name: 'الیاس پورمعتضدی',
    time: today.getHours() + ':' + today.getMinutes(),
    date: new Date().toLocaleDateString('fa-IR'),
    upVote: 0,
    downVote: 0,
    content: '',
  });
  useEffect(() => {
    if (id) {
      getSinglePost(id);
    }
  }, [getSinglePost, id]);
  const handleSubmit = (event) => {
    event.preventDefault();
    addAnswer({ ...data, postId: +id }, +id);
  };
  const handleCreate = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  return !loading ? (
    <div className="container mx-auto">
      <Card type="answer" post={singlePost} />
      <p className="text-3xl font-bold my-8">پاسخ خود را ثبت کنید</p>
      <form onSubmit={handleSubmit}>
        <label htmlFor="content">پاسخ خود را بنویسید</label>
        <textarea
          name="content"
          className="w-full my-3 resize-none  border py-2 px-3 rounded"
          id="content"
          cols="30"
          rows="10"
          onChange={handleCreate}
          placeholder="متن پاسخ"
        ></textarea>
        <input
          value="ارسال پاسخ"
          className="my-9 w-56  inline-block px-3 py-2.5 border bg-teal-800  text-white font-medium text-xs leading-tight uppercase rounded-md  hover:bg-teal-700 hover:shadow-lg  hover:text-white focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out"
          type="submit"
        />
      </form>
    </div>
  ) : (
    <Loading />
  );
};

SinglePost.prototype = {
  singlePost: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  getSinglePost: PropTypes.func.isRequired,
  addAnswer: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  singlePost: state.post.singlePost,
  loading: state.post.loading,
});

export default connect(mapStateToProps, { getSinglePost, addAnswer })(
  SinglePost
);
