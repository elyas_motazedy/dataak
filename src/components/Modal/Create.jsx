import { useState } from 'react';
import Modal from '@/utils/Modal';
import { XIcon } from '@heroicons/react/solid';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '@/redux/actions/post';
const today = new Date();
const CreateModal = ({ showModal, setShowModal, createPost }) => {
  const [data, setData] = useState({
    title: '',
    time: today.getHours() + ':' + today.getMinutes(),
    date: new Date().toLocaleDateString('fa-IR'),
    comments: Math.floor(Math.random() * 25),
    content: '',
  });

  const handleCreate = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    createPost(data);
  };

  return (
    <Modal showModal={showModal}>
      <form onSubmit={handleSubmit}>
        <div className="flex justify-center">
          <div className="block rounded-lg border bg-white w-full text-center">
            <div className="flex items-center space-x-1 justify-between">
              <div
                className={`group  px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2`}
              >
                <span className=" mx-5 font-bold my-2">ایجاد سوال جدید</span>
              </div>

              <div
                className={`group  px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2 justify-end`}
              >
                <p>
                  <XIcon
                    onClick={() => setShowModal(false)}
                    className="w-5 h-5 cursor-pointer"
                  />
                </p>
              </div>
            </div>
            <div className="p-4 bg-gray-100 text-right">
              <label htmlFor="title"> موضوع</label>
              <input
                onChange={handleCreate}
                type="text"
                className="w-full my-3 border py-2 px-3 rounded"
                name="title"
                id="title"
                placeholder="لطفا عنوان سوال خود را بنویسید"
              />
              <label htmlFor="content"> متن سوال</label>
              <textarea
                name="content"
                className="w-full my-3 resize-none  border py-2 px-3 rounded"
                id="content"
                cols="30"
                rows="10"
                onChange={handleCreate}
                placeholder="متن سوال"
              ></textarea>
              <div className="text-left">
                <button
                  type="button"
                  className=" inline-block px-3 py-2.5   text-teal-800 font-medium text-xs leading-tight uppercase rounded-md  hover:bg-teal-700 hover:shadow-lg  hover:text-white focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out"
                  onClick={() => setShowModal(false)}
                >
                  انصراف
                </button>
                {data.title === '' || data.content === '' ? (
                  <input
                    value="ایجاد سوال"
                    className=" inline-block px-3 py-2.5 border bg-gray-500  text-white font-medium text-xs leading-tight uppercase rounded-md  hover:bg-gray-700 hover:shadow-lg  hover:text-white focus:bg-gray-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out"
                    type="submit"
                    disabled
                  />
                ) : (
                  <input
                    value="ایجاد سوال"
                    className=" inline-block px-3 py-2.5 border bg-teal-800  text-white font-medium text-xs leading-tight uppercase rounded-md  hover:bg-teal-700 hover:shadow-lg  hover:text-white focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out"
                    type="submit"
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </form>
    </Modal>
  );
};

CreateModal.prototype = {
  createPost: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { createPost })(CreateModal);
