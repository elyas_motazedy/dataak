import { ChatIcon } from '@heroicons/react/solid';
import { toFarsiNumber } from '@/utils/helper';
import Link from 'next/link';
const QuestionCard = ({ post, showButton }) => {
  return (
    <div className="flex justify-center my-5">
      <div className="block rounded-lg border bg-white w-full text-center">
        <div className="flex items-center space-x-1 justify-between">
          <div
            className={`group px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2`}
          >
            <img
              width="35"
              height="35"
              className="rounded"
              src="/pp.jpg"
              alt="Neil image"
            />
            <span className=" mx-5 font-bold">{post.title}</span>
          </div>

          <div
            className={`group  px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2 justify-end`}
          >
            <p>
              <span className="text-xs text-gray-500">ساعت : </span>
              <span className="bold text-sm">
                {post.time && toFarsiNumber(post.time.split(':')[0])}:
                {post.time && toFarsiNumber(post.time.split(':')[1])}
              </span>
            </p>

            <p className=" border-r mx-4 px-3 border-gray-300">
              <span className="text-xs text-gray-500">تاریخ : </span>
              <span className="bold text-sm">{post.date}</span>
            </p>
            <p>
              <ChatIcon className="h-5 w-5 text-gray-500 inline" />
              <span className="text-xs ">
                {' '}
                {post.answers && toFarsiNumber(post.answers.length)}
              </span>
            </p>
          </div>
        </div>

        <div className="p-4 bg-gray-100 text-right">
          <p className="text-gray-700 text-base mb-1 ">{post.content}</p>
          {showButton && (
            <div className="text-left">
              <Link href={`/post/${post.id}`}>
                <a className=" inline-block px-3 py-2.5 border border-teal-800  text-teal-800 font-medium text-xs leading-tight uppercase rounded  hover:bg-teal-700 hover:shadow-lg  hover:text-white focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out">
                  مشاهده جزییات
                </a>
              </Link>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default QuestionCard;
