import { EmojiHappyIcon, EmojiSadIcon } from '@heroicons/react/solid';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { vote } from '@/redux/actions/post';
import { toFarsiNumber } from '@/utils/helper';
import Question from '../Question';
const AnswerCard = ({ post, vote }) => {
  const handleVote = (answer, type) => {
    let newData = {};
    if (type === 'up') {
      newData = { ...answer, upVote: answer.upVote + 1 };
    } else if (type === 'down') {
      newData = { ...answer, downVote: answer.downVote + 1 };
    }
    console.log(newData);
    vote(newData);
  };
  return (
    <div>
      <Question post={post} showButton={false} />
      <p className="text-3xl font-bold my-8">پاسخ ها</p>

      {post &&
        post.answers &&
        post.answers.map((answer) => (
          <div key={answer.id} className="flex justify-center my-5">
            <div className="block rounded-lg border bg-white w-full text-center">
              <div className="flex items-center space-x-1 justify-between">
                <div
                  className={`group px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2`}
                >
                  <img
                    width="35"
                    height="35"
                    className="rounded"
                    src="/pp.jpg"
                    alt="Neil image"
                  />
                  <span className=" mx-5 font-bold">{answer.name}</span>
                </div>

                <div
                  className={`group  px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 basis-1/2 justify-end`}
                >
                  <p>
                    <span className="text-xs text-gray-500">ساعت : </span>
                    <span className="bold text-sm">
                      {answer.time && toFarsiNumber(answer.time.split(':')[0])}:
                      {answer.time && toFarsiNumber(answer.time.split(':')[1])}
                    </span>
                  </p>

                  <p className=" border-r mx-4 px-3 border-gray-300">
                    <span className="text-xs text-gray-500">تاریخ : </span>
                    <span className="bold text-sm">{answer.date}</span>
                  </p>
                  <p className="mx-3">
                    <EmojiHappyIcon className="h-5 w-5 text-green-600 inline" />
                    <span className="text-xs ">
                      {' '}
                      {toFarsiNumber(answer.upVote)}
                    </span>
                  </p>
                  <p className="mx-3">
                    <EmojiSadIcon className="h-5 w-5 text-gray-400 inline" />
                    <span className="text-xs ">
                      {' '}
                      {toFarsiNumber(answer.downVote)}
                    </span>
                  </p>
                </div>
              </div>
              <div className="p-4 bg-gray-100 text-right">
                <p className="text-gray-700 text-base mb-1">{answer.content}</p>
                <div className="text-left">
                  <button
                    onClick={() => handleVote(answer, 'up')}
                    className="rounded inline-flex items-center border py-1.5 px-2"
                  >
                    <EmojiHappyIcon className="h-5 w-5 mx-1 text-green-600" />
                    <span className="text-xs  text-center mx-2 text-green-600">
                      پاسخ خوب بود
                    </span>
                  </button>
                  <button
                    onClick={() => handleVote(answer, 'down')}
                    className="rounded inline-flex items-center border  mx-5 text-white py-1.5 px-2"
                  >
                    <EmojiSadIcon className="h-5 w-5 mx-1 text-red-600" />
                    <span className="text-xs  text-center mx-2 text-red-600">
                      پاسخ خوب نبود
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

AnswerCard.prototype = {
  vote: PropTypes.func.isRequired,
};
// const mapStateToProps = (state) => ({
//   singlePost: state.post.singlePost,
//   loading: state.post.loading,
// });

export default connect(null, { vote })(AnswerCard);
