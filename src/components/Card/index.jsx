import Answer from './Answer';
import Question from './Question';
const Card = ({ post, type }) => {
  return (
    <div className="container mx-auto">
      {type === 'answer' ? (
        <Answer post={post} />
      ) : (
        <Question post={post} showButton={true} />
      )}
    </div>
  );
};

export default Card;
