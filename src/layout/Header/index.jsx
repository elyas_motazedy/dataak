import { useState } from 'react';
import CreateModal from '@/components/Modal/Create';
import { ChevronDownIcon, PlusSmIcon } from '@heroicons/react/solid';
import { useRouter } from 'next/router';
const Header = () => {
  const [showModal, setShowModal] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  return (
    <nav className="bg-white">
      <div className="mx-auto px-6">
        <div className="flex justify-between">
          <div className="flex space-x-4">
            <div>
              <div className="flex items-center py-1 h-full px-2 text-gray-700 hover:text-gray-900">
                <p className="font-extrabold ml-2 text-2xl">
                  {router.route === '/post/[id]'
                    ? 'جزییات سوال'
                    : 'لیست سوالات'}
                </p>
              </div>
            </div>
          </div>

          <div className="flex items-center space-x-1">
            <div className="w-full ">
              <div className="relative">
                <div
                  className={`group  px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75`}
                >
                  <button
                    onClick={() => setShowModal(true)}
                    className="rounded inline-flex items-center bg-teal-600 mx-5 text-white py-1.5 px-2"
                  >
                    <PlusSmIcon className="h-5 w-5 mx-1" />
                    <span className="text-xs  text-center mx-2">سوال جدید</span>
                  </button>
                  <img
                    width="40"
                    height="40"
                    className="rounded-full"
                    src="/pp.jpg"
                    alt="Neil image"
                  />
                  <span className=" mx-5">الیاس معتضدی</span>
                  <ChevronDownIcon
                    className={`ml-2 h-5 w-5 text-gray-600 group-hover:text-opacity-80 transition ease-in-out duration-150`}
                    aria-hidden="true"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <CreateModal showModal={showModal} setShowModal={setShowModal} />
    </nav>
  );
};

export default Header;
