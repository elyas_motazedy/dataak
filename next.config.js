/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  serverRuntimeConfig: {
    baseApiURL: process.env.BASE_API_URL,
  },
  publicRuntimeConfig: {
    baseApiURL: process.env.BASE_API_URL,
  },
  env: {
    baseApiURL: process.env.BASE_API_URL,
  },
};

module.exports = nextConfig;
